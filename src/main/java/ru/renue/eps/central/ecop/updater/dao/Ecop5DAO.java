package ru.renue.eps.central.ecop.updater.dao;

import org.springframework.stereotype.Repository;
import ru.renue.eps.central.ecop.updater.entities.Ecop5;

/**
 * Created by kgn on 05.03.2015.
 */
@Repository
public class Ecop5DAO extends AbstractEcopDAO<Ecop5> {

    public Ecop5DAO() {
        super(Ecop5.class);
    }
}
