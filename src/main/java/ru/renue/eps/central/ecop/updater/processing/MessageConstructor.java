package ru.renue.eps.central.ecop.updater.processing;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.stereotype.Service;
import ru.kontur.fts.eps.schemas.common.*;

import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

/**
 * Created by kgn on 04.03.2015.
 */
@Service
public class MessageConstructor {

    private final Logger logger = LoggerFactory.getLogger(MessageConstructor.class);

    private final Marshaller marshaller;

    private final String senderInformation;

    public MessageConstructor(final Marshaller marshaller) {
        this.marshaller = marshaller;
        this.senderInformation = "WMQ://RU.FTS.INT.EPSADM/EPS.ADMIN.INCOME";
    }

    public <T> byte[] constructMessage(T data, RoutingInfo routingInfo, String messageType) throws IOException {
        EnvelopeType envelope = new EnvelopeType();

        HeaderType header = new HeaderType();
        envelope.setHeader(header);

        ApplicationInfType applicationInf = new ApplicationInfType();
        RoutingInfType routingInf = new RoutingInfType();
        header.setAnyList(Arrays.<Object>asList(applicationInf, routingInf));

        applicationInf.setSoftKind("ECOP Updater");
        applicationInf.setMessageKind(messageType);

        String envelopeId = UUID.randomUUID().toString();
        routingInf.setEnvelopeID(envelopeId);
        routingInf.setPreparationDateTime(DateTime.now());
        routingInf.setReceiverInformationList(Arrays.asList(routingInfo.getTransportInfo().toString()));
        routingInf.setSenderInformation(senderInformation);

        BodyType body = new BodyType();
        envelope.setBody(body);
        body.setAnyList(Collections.<Object>singletonList(data));

        logger.info(String.format("Софрмировали сообщение #%s для пересылки в %s", envelopeId,
                                  routingInfo.getTransportInfo().toString()));

        return marshall(envelope);
    }

    public byte[] marshall(Object obj) throws IOException {
        ByteArrayOutputStream bas = new ByteArrayOutputStream();
        StreamResult sr = new StreamResult(bas);
        marshaller.marshal(obj, sr);
        bas.flush();

        return bas.toByteArray();
    }
}
