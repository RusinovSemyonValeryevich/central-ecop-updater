package ru.renue.eps.central.ecop.updater.entities;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 31.01.2020
 * Time: 11:10
 */
@Data
public class D44sfoivId implements Serializable {

    private String docCode;

    private String documentModeId;

    private Date beginDate;

}
