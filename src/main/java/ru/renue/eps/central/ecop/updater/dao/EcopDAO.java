package ru.renue.eps.central.ecop.updater.dao;

import org.springframework.stereotype.Repository;
import ru.renue.eps.central.ecop.updater.entities.Ecop;

/**
 * Created by kgn on 05.03.2015.
 */
@Repository
public class EcopDAO extends AbstractEcopDAO<Ecop> {
    EcopDAO() {
        super(Ecop.class);
    }
}
