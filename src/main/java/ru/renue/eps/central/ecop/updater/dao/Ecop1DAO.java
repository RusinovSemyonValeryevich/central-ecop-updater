package ru.renue.eps.central.ecop.updater.dao;

import org.springframework.stereotype.Repository;
import ru.renue.eps.central.ecop.updater.entities.Ecop1;

/**
 * Created by kgn on 05.03.2015.
 */
@Repository
public class Ecop1DAO extends AbstractEcopDAO<Ecop1> {

    public Ecop1DAO() {
        super(Ecop1.class);
    }
}
