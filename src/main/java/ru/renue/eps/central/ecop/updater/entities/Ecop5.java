package ru.renue.eps.central.ecop.updater.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by kgn on 05.03.2015.
 */
@Entity
@Table(name = "ECOP5")
@Data
public class Ecop5 extends AbstractEcop {

    @Column(name = "P_LOCAT", length = 255)
    private String pLocat;

    @Column(name = "TAM_LOC", length = 8)
    private String tamLoc;
}
