package ru.renue.eps.central.ecop.updater.converter;

import org.joda.time.LocalDate;
import ru.kontur.fts.eps.schemas.nci.ecopcatalog.ECOProwType;
import ru.renue.eps.central.ecop.updater.entities.Ecop;

/**
 * Created by kgn on 05.03.2015.
 */
public class EcopConverter extends Entity2DtoConverter<Ecop, ECOProwType> {

    @Override
    public ECOProwType convert(Ecop entity) {
        if (entity == null) {
            return null;
        }

        ECOProwType dto = new ECOProwType();
        dto.setCITY(entity.getCity());
        if (entity.getDatBegDoc() != null) {
            dto.setDATBEGDOC(new LocalDate(entity.getDatBegDoc().getTime()));
        }
        if (entity.getDateMod() != null) {
            dto.setDATEMOD(new LocalDate(entity.getDateMod().getTime()));
        }
        if (entity.getDateEndDoc() != null) {
            dto.setDATENDDOC(new LocalDate(entity.getDateEndDoc().getTime()));
        }
        if (entity.getDateSusDoc() != null) {
            dto.setDATSUSDOC(new LocalDate(entity.getDateSusDoc().getTime()));
        }
        if (entity.getDBegin() != null) {
            dto.setDBEGIN(new LocalDate(entity.getDBegin().getTime()));
        }
        if (entity.getDEnd() != null) {
            dto.setDEND(new LocalDate(entity.getDEnd().getTime()));
        }
        dto.setEMAIL(entity.getEmail());
        dto.setINN(entity.getInn());
        dto.setKPP(entity.getKpp());
        dto.setNBLANK(entity.getNBlank());
        dto.setNLIC(entity.getNlic());
        dto.setNUMBEGDOC(entity.getNumBegDoc());
        dto.setNUMENDDOC(entity.getNumEndDoc());
        dto.setNUMSUSDOC(entity.getNumSusDoc());
        dto.setOGRN(entity.getOgrn());
        dto.setOKPO(entity.getOkpo());
        dto.setOPFVL(entity.getOpfVl());
        dto.setOSP(entity.getOsp());
        dto.setOWNER(entity.getOwner());
        dto.setPOST(entity.getPost());
        dto.setPRPER(entity.getPrPer());
        dto.setST(entity.getSt());
        dto.setSTREET(entity.getStreet());
        dto.setSUBD(entity.getSubD());
        dto.setTELEFAX(entity.getTelefax());
        dto.setTELEPHON(entity.getTelephon());
        dto.setTELETYPE(entity.getTeletype());
        dto.setWEBSITE(entity.getWebSite());

        return dto;
    }
}
