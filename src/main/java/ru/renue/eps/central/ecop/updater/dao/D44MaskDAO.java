package ru.renue.eps.central.ecop.updater.dao;

import org.springframework.stereotype.Repository;
import ru.renue.eps.central.ecop.updater.entities.D44Mask;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by azhebko on 29.01.2016.
 */
@Repository
public class D44MaskDAO {

    @PersistenceContext
    private EntityManager em;

    public List<D44Mask> findAll() {
        return em.createQuery("select e from " + D44Mask.class.getSimpleName() + " e", D44Mask.class).getResultList();
    }
}
