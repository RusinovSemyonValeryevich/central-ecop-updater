package ru.renue.eps.central.ecop.updater.dao;

import org.springframework.stereotype.Repository;
import ru.renue.eps.central.ecop.updater.entities.D44mtn;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by azhebko on 29.01.2016.
 */
@Repository
public class D44mtnDAO {

    @PersistenceContext
    private EntityManager em;

    public List<D44mtn> findAll() {
        return em.createQuery("select e from " + D44mtn.class.getSimpleName() + " e", D44mtn.class).getResultList();
    }
}
