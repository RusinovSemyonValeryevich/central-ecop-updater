package ru.renue.eps.central.ecop.updater.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by azhebko on 29.01.2016.
 */
@Entity
@Table(name = "D44_M_TN")
@IdClass(D44mtnId.class)
@Data
public class D44mtn {

    @Id
    @Column(name = "KOD")
    private String kod;

    @Id
    @Column(name = "N_MSK")
    private Integer nMsk;

    @Id
    @Column(name = "KODT")
    private String kodt;

    @Column(name = "SID_SMEV")
    private String sidSmev;

    @Column(name = "FOIV")
    private String foiv;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATBEG")
    private Date datbeg;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATEND")
    private Date datend;
}
