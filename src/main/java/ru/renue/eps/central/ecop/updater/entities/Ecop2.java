package ru.renue.eps.central.ecop.updater.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ECOP2")
@Data
public class Ecop2 extends AbstractEcop {

    @Column(name = "KOD_TAM", length = 8)
    private String kodTam;

    @Column(name = "SP_SIMPL", length = 1)
    private String spSimpl;

    @Column(name = "TIME_RESH")
    private Double timeResh;
}
