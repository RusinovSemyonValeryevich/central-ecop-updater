package ru.renue.eps.central.ecop.updater.converter;

import org.joda.time.LocalDate;
import ru.kontur.fts.eps.schemas.nci.d44mask.D44mtnRowType;
import ru.renue.eps.central.ecop.updater.entities.D44mtn;

/**
 * Created by azhebko on 29.01.2016.
 */
public class D44mtnConverter extends Entity2DtoConverter<D44mtn, D44mtnRowType> {

    @Override
    public D44mtnRowType convert(D44mtn entity) {
        if (entity == null) {
            return null;
        }
        D44mtnRowType dto = new D44mtnRowType();

        dto.setKOD(entity.getKod());
        dto.setNMSK(entity.getNMsk());
        dto.setKODT(entity.getKodt());
        dto.setSIDSMEV(entity.getSidSmev());
        dto.setFOIV(entity.getFoiv());
        if (entity.getDatbeg() != null) {
            dto.setDATBEG(LocalDate.fromDateFields(entity.getDatbeg()));
        }

        if (entity.getDatend() != null) {
            dto.setDATEND(LocalDate.fromDateFields(entity.getDatend()));
        }

        return dto;
    }
}

