package ru.renue.eps.central.ecop.updater.processing;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.kontur.fts.eps.schemas.nci.svhcatalog.SvhCatalogType;
import ru.renue.eps.central.ecop.updater.converter.SvhConverter;
import ru.renue.eps.central.ecop.updater.dao.SvhDAO;
import ru.renue.eps.central.ecop.updater.entities.Svh;
import ru.renue.eps.central.ecop.updater.transport.MessageSender;

import java.util.List;
import java.util.UUID;

@Service
public class SvhUpdater extends Updater {

    private final SvhDAO svhDAO;

    public SvhUpdater(final @Qualifier("svhRoutingInfos") List<RoutingInfo> routingInfos,
                      final MessageConstructor messageConstructor, final MessageSender messageSender, final SvhDAO svhDAO) {
        super(routingInfos, messageConstructor, messageSender, "ADM.00103");
        this.svhDAO = svhDAO;
        logger = LoggerFactory.getLogger(SvhUpdater.class);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    @Scheduled(cron = "0 5 0 * * *")
    public void process() {
        List<Svh> svhList = svhDAO.findAll();

        SvhConverter svhConverter = new SvhConverter();
        SvhCatalogType svhCatalog = new SvhCatalogType();
        svhCatalog.setDocumentID(UUID.randomUUID().toString());
        svhCatalog.setSvhRowList(svhConverter.convert(svhList));

        update(svhCatalog);
    }
}
