package ru.renue.eps.central.ecop.updater.converter;

import org.joda.time.LocalDate;
import ru.kontur.fts.eps.schemas.nci.d44sfoiv.D44SFOIVrowType;
import ru.renue.eps.central.ecop.updater.entities.D44sfoiv;

/**
 * Created by kgn on 23.10.2015.
 */
public class D44sfoivConverter extends Entity2DtoConverter<D44sfoiv, D44SFOIVrowType> {

    @Override
    public D44SFOIVrowType convert(D44sfoiv entity) {
        if (entity == null) {
            return null;
        }
        D44SFOIVrowType dto = new D44SFOIVrowType();
        dto.setKOD(entity.getDocCode());
        dto.setMODEID(entity.getDocumentModeId());
        if (entity.getBeginDate() != null) {
            dto.setDATBEG(new LocalDate(entity.getBeginDate().getTime()));
        }
        if (entity.getEndDate() != null) {
            dto.setDATEND(new LocalDate(entity.getEndDate().getTime()));
        }

        dto.setDOCNAME(entity.getDocName());
        dto.setKODFOIV(entity.getFoivCode());
        dto.setSIDSMEV(entity.getFoivSid());
        dto.setNUMBEGDOC(entity.getNumBegDoc());
        if (entity.getDatBegDoc() != null) {
            dto.setDATBEGDOC(new LocalDate(entity.getDatBegDoc().getTime()));
        }

        return dto;
    }
}
