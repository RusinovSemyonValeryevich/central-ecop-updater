package ru.renue.eps.central.ecop.updater.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.renue.eps.central.ecop.updater.processing.RoutingInfo;
import ru.renue.eps.central.ecop.updater.transport.TransportInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 31.01.2020
 * Time: 8:59
 */
@Configuration
public class RoutingInfoConfiguration {


    @Bean
    public List<RoutingInfo> d44MaskRoutingInfos() {
        return new ArrayList<RoutingInfo>() {
            {
                add(cittu());
                add(ctu());
                add(sztu());
                add(yutu());
                add(ptu());
                add(utu());
                add(stu());
                add(dvtu());
                add(sktu());
            }
        };
    }

    @Bean
    public RoutingInfo cittu() {
        RoutingInfo routingInfo = new RoutingInfo();
        TransportInfo transportInfo = new TransportInfo();
        transportInfo.setQueue("EPS.ADMIN.FROM");
        transportInfo.setQueueManager("RU.FTS.CITTU.EPS");
        routingInfo.setTransportInfo(transportInfo);
        return routingInfo;
    }

    @Bean
    public RoutingInfo ctu() {
        RoutingInfo routingInfo = new RoutingInfo();
        TransportInfo transportInfo = new TransportInfo();
        transportInfo.setQueue("EPS.ADMIN.FROM");
        transportInfo.setQueueManager("RU.FTS.CTU");
        routingInfo.setTransportInfo(transportInfo);
        return routingInfo;
    }

    @Bean
    public RoutingInfo sztu() {
        RoutingInfo routingInfo = new RoutingInfo();
        TransportInfo transportInfo = new TransportInfo();
        transportInfo.setQueue("EPS.ADMIN.FROM");
        transportInfo.setQueueManager("RU.FTS.SZTU");
        routingInfo.setTransportInfo(transportInfo);
        return routingInfo;
    }

    @Bean
    public RoutingInfo yutu() {
        RoutingInfo routingInfo = new RoutingInfo();
        TransportInfo transportInfo = new TransportInfo();
        transportInfo.setQueue("EPS.ADMIN.FROM");
        transportInfo.setQueueManager("RU.FTS.YUTU");
        routingInfo.setTransportInfo(transportInfo);
        return routingInfo;
    }

    @Bean
    public RoutingInfo ptu() {
        RoutingInfo routingInfo = new RoutingInfo();
        TransportInfo transportInfo = new TransportInfo();
        transportInfo.setQueue("EPS.ADMIN.FROM");
        transportInfo.setQueueManager("RU.FTS.PTU");
        routingInfo.setTransportInfo(transportInfo);
        return routingInfo;
    }

    @Bean
    public RoutingInfo utu() {
        RoutingInfo routingInfo = new RoutingInfo();
        TransportInfo transportInfo = new TransportInfo();
        transportInfo.setQueue("EPS.ADMIN.FROM");
        transportInfo.setQueueManager("RU.FTS.UTU");
        routingInfo.setTransportInfo(transportInfo);
        return routingInfo;
    }

    @Bean
    public RoutingInfo stu() {
        RoutingInfo routingInfo = new RoutingInfo();
        TransportInfo transportInfo = new TransportInfo();
        transportInfo.setQueue("EPS.ADMIN.FROM");
        transportInfo.setQueueManager("RU.FTS.STU");
        routingInfo.setTransportInfo(transportInfo);
        return routingInfo;
    }

    @Bean
    public RoutingInfo dvtu() {
        RoutingInfo routingInfo = new RoutingInfo();
        TransportInfo transportInfo = new TransportInfo();
        transportInfo.setQueue("EPS.ADMIN.FROM");
        transportInfo.setQueueManager("RU.FTS.DVTU");
        routingInfo.setTransportInfo(transportInfo);
        return routingInfo;
    }

    @Bean
    public RoutingInfo sktu() {
        RoutingInfo routingInfo = new RoutingInfo();
        TransportInfo transportInfo = new TransportInfo();
        transportInfo.setQueue("EPS.ADMIN.FROM");
        transportInfo.setQueueManager("RU.FTS.SKTU");
        routingInfo.setTransportInfo(transportInfo);
        return routingInfo;
    }

    @Bean
    public List<RoutingInfo> d44foivRoutingInfos() {
        return new ArrayList<RoutingInfo>() {
            {
                add(cittu());
                add(ctu());
                add(sztu());
                add(yutu());
                add(ptu());
                add(utu());
                add(stu());
                add(dvtu());
                add(sktu());
                add(ead());
                add(pi());
            }
        };
    }

    @Bean
    public RoutingInfo ead() {
        RoutingInfo routingInfo = new RoutingInfo();
        TransportInfo transportInfo = new TransportInfo();
        transportInfo.setQueue("EAD.EPSADM.FROM");
        transportInfo.setQueueManager("RU.FTS.CITTU");
        routingInfo.setTransportInfo(transportInfo);
        return routingInfo;
    }

    @Bean
    public RoutingInfo pi() {
        RoutingInfo routingInfo = new RoutingInfo();
        TransportInfo transportInfo = new TransportInfo();
        transportInfo.setQueue("EPS.ADMIN");
        transportInfo.setQueueManager("RU.FTS.CITTU.DPCJ");
        routingInfo.setTransportInfo(transportInfo);
        return routingInfo;
    }

    @Bean
    public List<RoutingInfo> ecopRoutingInfos() {
        return new ArrayList<RoutingInfo>() {
            {
                add(cittu());
                add(ctu());
                add(sztu());
                add(yutu());
                add(ptu());
                add(utu());
                add(stu());
                add(dvtu());
                add(sktu());
                add(admin());
            }
        };
    }

    @Bean
    public RoutingInfo admin() {
        RoutingInfo routingInfo = new RoutingInfo();
        TransportInfo transportInfo = new TransportInfo();
        transportInfo.setQueue("RU.FTS.INT.EPSADM");
        transportInfo.setQueueManager("GWA.INCOME");
        routingInfo.setTransportInfo(transportInfo);
        return routingInfo;
    }

    @Bean
    public List<RoutingInfo> svhRoutingInfos() {
        return new ArrayList<RoutingInfo>() {
            {
                add(admin());
            }
        };
    }
}
