package ru.renue.eps.central.ecop.updater.dao;

import org.springframework.stereotype.Repository;
import ru.renue.eps.central.ecop.updater.entities.Ecop2;

/**
 * Created by kgn on 05.03.2015.
 */
@Repository
public class Ecop2DAO extends AbstractEcopDAO<Ecop2> {

    public Ecop2DAO() {
        super(Ecop2.class);
    }
}
