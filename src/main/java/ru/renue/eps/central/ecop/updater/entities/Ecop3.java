package ru.renue.eps.central.ecop.updater.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ECOP3")
@Data
public class Ecop3 extends AbstractEcop {
    @Column(name = "SP_SIMPL", length = 1)
    private String spSimpl;
}
