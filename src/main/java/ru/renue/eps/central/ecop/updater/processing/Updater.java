package ru.renue.eps.central.ecop.updater.processing;

import org.slf4j.Logger;
import ru.renue.eps.central.ecop.updater.transport.MessageSender;
import ru.renue.eps.central.ecop.updater.transport.TransportException;

import java.io.IOException;
import java.util.List;

/**
 * Created by kgn on 23.10.2015.
 */
public abstract class Updater {

    private final List<RoutingInfo> routingInfos;

    private final MessageConstructor messageConstructor;

    private final MessageSender messageSender;

    private final String messageType;

    protected Logger logger;

    protected Updater(final List<RoutingInfo> routingInfos, final MessageConstructor messageConstructor,
                      final MessageSender messageSender, final String messageType) {
        this.routingInfos = routingInfos;
        this.messageConstructor = messageConstructor;
        this.messageSender = messageSender;
        this.messageType = messageType;
    }

    public <T> void update(T data) {
        for (RoutingInfo routingInfo : routingInfos) {
            try {
                byte[] message = messageConstructor.constructMessage(data, routingInfo, messageType);
                messageSender.send(message, routingInfo.getTransportInfo());
            }
            catch (IOException e) {
                logger.error("Ошибка создания сообщения", e);
                throw new RuntimeException(e);
            }
            catch (TransportException e) {
                logger.error("Ошибка при отправке сообщения", e);
                throw new RuntimeException(e);
            }
        }
    }

    public abstract void process();
}
