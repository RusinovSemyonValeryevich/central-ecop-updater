package ru.renue.eps.central.ecop.updater.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SKLADBX")
@Data
public class Svh {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", allocationSize = 1, sequenceName = "svh_sequence")
    @Column(name = "ID")
    private Long id;

    @Column(name = "ST", length = 2)
    private String st;

    @Column(name = "KODT", length = 8)
    private String kodt;

    @Column(name = "NAMT", length = 30)
    private String namt;

    @Column(name = "ITAM", length = 1)
    private String itam;

    @Column(name = "TYP_DOC", length = 1)
    private String typDoc;

    @Column(name = "NLIC", length = 25)
    private String nlic;

    @Column(name = "PR_PER", length = 1)
    private String prPer;

    @Column(name = "DBEGIN")
    @Temporal(TemporalType.DATE)
    private Date dBegin;

    @Column(name = "DEND")
    @Temporal(TemporalType.DATE)
    private Date dEnd;

    @Column(name = "IDEL", length = 1)
    private String idel;

    @Column(name = "TYPES", length = 1)
    private String types;

    @Column(name = "AREAS")
    private double areas;

    @Column(name = "VOLUME")
    private double volume;

    @Column(name = "OWNER", length = 255)
    private String owner;

    @Column(name = "ADROWN", length = 4000)
    private String adrown;

    @Column(name = "TELEFON", length = 50)
    private String telefon;

    @Column(name = "WEBSITE", length = 100)
    private String website;

    @Column(name = "EMAIL", length = 100)
    private String email;

    @Column(name = "ADRS", length = 4000)
    private String adrs;

    @Column(name = "TELEFS", length = 30)
    private String telefs;

    @Column(name = "IAKCIZ", length = 1)
    private String iakciz;

    @Column(name = "ASOVAM", length = 8)
    private String asovam;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_MOD")
    private Date dateMod;

    @Column(name = "VIDTRANS", length = 30)
    private String vidtrans;

    @Column(name = "PR441PRIL3", length = 1)
    private String pr441pril3;

    @Column(name = "ATA", length = 1)
    private String ata;

    @Temporal(TemporalType.DATE)
    @Column(name = "D_ISL")
    private Date dIsl;

    @Column(name = "OGRN", length = 15)
    private String ogrn;

    @Column(name = "INN", length = 20)
    private String inn;

    @Column(name = "KPP", length = 9)
    private String kpp;
}
