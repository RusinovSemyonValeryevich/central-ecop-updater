package ru.renue.eps.central.ecop.updater.converter;

import org.joda.time.LocalDate;
import ru.kontur.fts.eps.schemas.nci.ecopcatalog.ECOP3RowType;
import ru.renue.eps.central.ecop.updater.entities.Ecop3;

/**
 * Created by kgn on 05.03.2015.
 */
public class Ecop3Converter extends Entity2DtoConverter<Ecop3, ECOP3RowType> {

    @Override
    public ECOP3RowType convert(Ecop3 entity) {
        if (entity == null) {
            return null;
        }
        ECOP3RowType dto = new ECOP3RowType();

        if (entity.getDBegin() != null) {
            dto.setDBEGIN(new LocalDate(entity.getDBegin().getTime()));
        }
        dto.setNLIC(entity.getNlic());
        dto.setPRPER(entity.getPrPer());
        dto.setST(entity.getSt());

        dto.setSPSIMPL(entity.getSpSimpl());

        return dto;
    }
}
