package ru.renue.eps.central.ecop.updater.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by kgn on 05.03.2015.
 */
public abstract class Entity2DtoConverter <E, D> {

    public List<D> convert(List<E> entities) {
        if (entities == null || entities.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList<D> dtos = new ArrayList<D>(entities.size());
        for (E entity : entities) {
            dtos.add(convert(entity));
        }
        return dtos;
    }

    public abstract D convert(E entity);
}
