package ru.renue.eps.central.ecop.updater.converter;

import org.joda.time.LocalDate;
import ru.kontur.fts.eps.schemas.nci.ecopcatalog.ECOP5RowType;
import ru.renue.eps.central.ecop.updater.entities.Ecop5;

/**
 * Created by kgn on 05.03.2015.
 */
public class Ecop5Converter extends Entity2DtoConverter<Ecop5, ECOP5RowType> {

    @Override
    public ECOP5RowType convert(Ecop5 entity) {
        if (entity == null) {
            return null;
        }

        ECOP5RowType dto = new ECOP5RowType();

        if (entity.getDBegin() != null) {
            dto.setDBEGIN(new LocalDate(entity.getDBegin().getTime()));
        }
        dto.setNLIC(entity.getNlic());
        dto.setPRPER(entity.getPrPer());
        dto.setST(entity.getSt());

        dto.setPLOCAT(entity.getPLocat());
        dto.setTAMLOC(entity.getTamLoc());

        return dto;
    }
}
