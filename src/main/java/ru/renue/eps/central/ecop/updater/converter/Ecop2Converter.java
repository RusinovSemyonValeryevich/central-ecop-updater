package ru.renue.eps.central.ecop.updater.converter;

import org.joda.time.LocalDate;
import ru.kontur.fts.eps.schemas.nci.ecopcatalog.ECOP2RowType;
import ru.renue.eps.central.ecop.updater.entities.Ecop2;

/**
 * Created by kgn on 05.03.2015.
 */
public class Ecop2Converter extends Entity2DtoConverter<Ecop2, ECOP2RowType> {

    @Override
    public ECOP2RowType convert(Ecop2 entity) {
        if (entity == null) {
            return null;
        }
        ECOP2RowType dto = new ECOP2RowType();
        if (entity.getDBegin() != null) {
            dto.setDBEGIN(new LocalDate(entity.getDBegin().getTime()));
        }
        dto.setNLIC(entity.getNlic());
        dto.setPRPER(entity.getPrPer());
        dto.setST(entity.getSt());

        dto.setKODTAM(entity.getKodTam());
        dto.setSPSIMPL(entity.getSpSimpl());
        dto.setTIMERESH(entity.getTimeResh());

        return dto;
    }
}
