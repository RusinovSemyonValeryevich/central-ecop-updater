package ru.renue.eps.central.ecop.updater.processing;

import lombok.Data;
import ru.renue.eps.central.ecop.updater.transport.TransportInfo;

/**
 * Created by kgn on 04.03.2015.
 */
@Data
public class RoutingInfo {

    private TransportInfo transportInfo;
}
