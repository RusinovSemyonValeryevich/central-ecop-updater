package ru.renue.eps.central.ecop.updater.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kgn on 23.10.2015.
 */
@Entity
@Table(name = "D44SFOIV")
@IdClass(D44sfoivId.class)
@Data
public class D44sfoiv {

    @Id
    @Column(name = "KOD")
    private String docCode;

    @Id
    @Column(name = "MODE_ID")
    private String documentModeId;

    @Id
    @Temporal(TemporalType.DATE)
    @Column(name = "DATBEG")
    private Date beginDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATEND")
    private Date endDate;

    @Column(name = "KOD_FOIV")
    private String foivCode;

    @Column(name = "DOCNAME")
    private String docName;

    @Column(name = "SID_SMEV")
    private String foivSid;

    @Column(name = "NUMBEGDOC")
    private String numBegDoc;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATBEGDOC")
    private Date datBegDoc;

}
