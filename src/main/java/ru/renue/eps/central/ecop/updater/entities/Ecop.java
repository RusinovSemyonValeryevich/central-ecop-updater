package ru.renue.eps.central.ecop.updater.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ECOP")
@Data
public class Ecop extends AbstractEcop {

    @Column(name = "N_BLANK", length = 10)
    private String nBlank;

    @Column(name = "DEND")
    @Temporal(TemporalType.DATE)
    private Date dEnd;

    @Column(name = "OWNER", length = 255)
    private String owner;

    @Column(name = "OPF_VL", length = 2)
    private String opfVl;

    @Column(name = "POST", length = 9)
    private String post;

    @Column(name = "SUBD", length = 50)
    private String subD;

    @Column(name = "CITY", length = 35)
    private String city;

    @Column(name = "STREET", length = 50)
    private String street;

    @Column(name = "TELEPHON", length = 100)
    private String telephon;

    @Column(name = "TELETYPE", length = 100)
    private String teletype;

    @Column(name = "TELEFAX", length = 100)
    private String telefax;

    @Column(name = "EMAIL", length = 100)
    private String email;

    @Column(name = "WEBSITE", length = 100)
    private String webSite;

    @Column(name = "OGRN", length = 15)
    private String ogrn;

    @Column(name = "INN", length = 20)
    private String inn;

    @Column(name = "KPP", length = 9)
    private String kpp;

    @Column(name = "OKPO", length = 10)
    private String okpo;

    @Column(name = "OSP", length = 1)
    private String osp;

    @Column(name = "DATE_MOD")
    @Temporal(TemporalType.DATE)
    private Date dateMod;

    @Column(name = "NUMBEGDOC", length = 20)
    private String numBegDoc;

    @Column(name = "DATBEGDOC")
    @Temporal(TemporalType.DATE)
    private Date datBegDoc;

    @Column(name = "NUMENDDOC", length = 20)
    private String numEndDoc;

    @Column(name = "DATENDDOC")
    @Temporal(TemporalType.DATE)
    private Date dateEndDoc;

    @Column(name = "NUMSUSDOC", length = 20)
    private String numSusDoc;

    @Column(name = "DATSUSDOC")
    @Temporal(TemporalType.DATE)
    private Date dateSusDoc;
}
