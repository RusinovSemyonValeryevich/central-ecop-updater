package ru.renue.eps.central.ecop.updater.converter;

import org.joda.time.LocalDate;
import ru.kontur.fts.eps.schemas.nci.ecopcatalog.ECOP4RowType;
import ru.renue.eps.central.ecop.updater.entities.Ecop4;

/**
 * Created by kgn on 05.03.2015.
 */
public class Ecop4Converter extends Entity2DtoConverter<Ecop4, ECOP4RowType> {

    @Override
    public ECOP4RowType convert(Ecop4 entity) {
        if (entity == null) {
            return null;
        }

        ECOP4RowType dto = new ECOP4RowType();

        if (entity.getDBegin() != null) {
            dto.setDBEGIN(new LocalDate(entity.getDBegin().getTime()));
        }
        dto.setNLIC(entity.getNlic());
        dto.setPRPER(entity.getPrPer());
        dto.setST(entity.getSt());

        if (entity.getAmount() != null) {
            dto.setAMOUNT(entity.getAmount().doubleValue());
        }
        if (entity.getBegdoctp() != null) {
            dto.setBEGDOCTP(new LocalDate(entity.getBegdoctp().getTime()));
        }
        if (entity.getDatbdoctp() != null) {
            dto.setDATBDOCTP(new LocalDate(entity.getDatbdoctp().getTime()));
        }
        if (entity.getEnddoctp() != null) {
            dto.setENDDOCTP(new LocalDate(entity.getEnddoctp().getTime()));
        }
        dto.setGUARKIND(entity.getGuarkind());
        dto.setNUMBDOCTP(entity.getNumbdoctp());

        return dto;
    }
}
