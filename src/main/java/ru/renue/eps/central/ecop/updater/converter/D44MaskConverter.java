package ru.renue.eps.central.ecop.updater.converter;

import org.joda.time.LocalDate;
import ru.kontur.fts.eps.schemas.nci.d44mask.D44MaskRowType;
import ru.renue.eps.central.ecop.updater.entities.D44Mask;

/**
 * Created by azhebko on 29.01.2016.
 */
public class D44MaskConverter extends Entity2DtoConverter<D44Mask, D44MaskRowType> {

    @Override
    public D44MaskRowType convert(D44Mask entity) {
        if (entity == null) {
            return null;
        }
        D44MaskRowType dto = new D44MaskRowType();

        dto.setKOD(entity.getKod());
        dto.setKMASKA(entity.getKMaska());
        dto.setNMSK(entity.getNMsk());
        dto.setSIDSMEV(entity.getSidSmev());
        dto.setNAMEMSK(entity.getNameMsk());
        dto.setDSCRMSK(entity.getDscrMsk());
        if (entity.getDatbeg() != null) {
            dto.setDATBEG(LocalDate.fromDateFields(entity.getDatbeg()));
        }

        dto.setNUMBEGDOC(entity.getNumbegdoc());
        if (entity.getDatbegdoc() != null) {
            dto.setDATBEGDOC(LocalDate.fromDateFields(entity.getDatbegdoc()));
        }

        if (entity.getDatend() != null) {
            dto.setDATEND(LocalDate.fromDateFields(entity.getDatend()));
        }

        dto.setNUMENDDOC(entity.getNumenddoc());
        if (entity.getDatenddoc() != null) {
            dto.setDATENDDOC(LocalDate.fromDateFields(entity.getDatenddoc()));
        }

        dto.setPRIZNAK(entity.getPriznak());
        dto.setPDOP(entity.getPDop());

        return dto;
    }
}

