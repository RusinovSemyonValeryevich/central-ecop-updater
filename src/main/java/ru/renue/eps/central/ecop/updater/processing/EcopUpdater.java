package ru.renue.eps.central.ecop.updater.processing;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.kontur.fts.eps.schemas.nci.ecopcatalog.EcopCatalogType;
import ru.renue.eps.central.ecop.updater.converter.*;
import ru.renue.eps.central.ecop.updater.dao.*;
import ru.renue.eps.central.ecop.updater.entities.*;
import ru.renue.eps.central.ecop.updater.transport.MessageSender;

import java.util.List;
import java.util.UUID;

/**
 * Created by kgn on 04.03.2015.
 */
@Service
public class EcopUpdater extends Updater {

    private final EcopDAO ecopDAO;

    private final Ecop1DAO ecop1DAO;

    private final Ecop2DAO ecop2DAO;

    private final Ecop3DAO ecop3DAO;

    private final Ecop4DAO ecop4DAO;

    private final Ecop5DAO ecop5DAO;

    public EcopUpdater(final EcopDAO ecopDAO, final Ecop1DAO ecop1DAO, final Ecop2DAO ecop2DAO, final Ecop3DAO ecop3DAO,
                       final Ecop4DAO ecop4DAO, final Ecop5DAO ecop5DAO,
                       final @Qualifier("ecopRoutingInfos") List<RoutingInfo> routingInfos,
                       final MessageConstructor messageConstructor, final MessageSender messageSender) {
        super(routingInfos, messageConstructor, messageSender, "ADM.00100");
        this.ecopDAO = ecopDAO;
        this.ecop1DAO = ecop1DAO;
        this.ecop2DAO = ecop2DAO;
        this.ecop3DAO = ecop3DAO;
        this.ecop4DAO = ecop4DAO;
        this.ecop5DAO = ecop5DAO;
        logger = LoggerFactory.getLogger(EcopUpdater.class);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Scheduled(cron = "0 5 0 * * *")
    public void process() {
        List<Ecop> ecops = ecopDAO.findAll();
        List<Ecop1> ecop1s = ecop1DAO.findAll();
        List<Ecop2> ecop2s = ecop2DAO.findAll();
        List<Ecop3> ecop3s = ecop3DAO.findAll();
        List<Ecop4> ecop4s = ecop4DAO.findAll();
        List<Ecop5> ecop5s = ecop5DAO.findAll();

        EcopConverter ecopConverter = new EcopConverter();
        Ecop1Converter ecop1Converter = new Ecop1Converter();
        Ecop2Converter ecop2Converter = new Ecop2Converter();
        Ecop3Converter ecop3Converter = new Ecop3Converter();
        Ecop4Converter ecop4Converter = new Ecop4Converter();
        Ecop5Converter ecop5Converter = new Ecop5Converter();

        EcopCatalogType ecopCatalog = new EcopCatalogType();
        ecopCatalog.setDocumentID(UUID.randomUUID().toString());

        ecopCatalog.setECOProwList(ecopConverter.convert(ecops));
        ecopCatalog.setECOP1rowList(ecop1Converter.convert(ecop1s));
        ecopCatalog.setECOP2rowList(ecop2Converter.convert(ecop2s));
        ecopCatalog.setECOP3rowList(ecop3Converter.convert(ecop3s));
        ecopCatalog.setECOP4rowList(ecop4Converter.convert(ecop4s));
        ecopCatalog.setECOP5rowList(ecop5Converter.convert(ecop5s));

        update(ecopCatalog);
    }
}
