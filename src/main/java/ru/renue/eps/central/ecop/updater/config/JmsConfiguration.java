package ru.renue.eps.central.ecop.updater.config;

import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.msg.client.wmq.WMQConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 31.01.2020
 * Time: 10:25
 */
@Configuration
public class JmsConfiguration {

    @Bean
    public JmsTemplate jmsTemplate(final ConnectionFactory jmsConnectionFactory) {
        JmsTemplate jmsTemplate = new JmsTemplate();
        jmsTemplate.setSessionTransacted(true);
        jmsTemplate.setConnectionFactory(jmsConnectionFactory);
        jmsTemplate.setSessionAcknowledgeModeName("AUTO_ACKNOWLEDGE");
        return jmsTemplate;
    }

    @Bean
    public ConnectionFactory jmsConnectionFactory() throws JMSException {
        MQQueueConnectionFactory factory = new MQQueueConnectionFactory();
        factory.setTransportType(WMQConstants.WMQ_CM_CLIENT);
        return factory;
    }
}
