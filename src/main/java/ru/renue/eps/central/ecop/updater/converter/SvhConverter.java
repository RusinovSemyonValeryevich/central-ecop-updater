package ru.renue.eps.central.ecop.updater.converter;

import org.joda.time.LocalDate;
import ru.kontur.fts.eps.schemas.nci.svhcatalog.SvhRowType;
import ru.renue.eps.central.ecop.updater.entities.Svh;

public class SvhConverter extends Entity2DtoConverter<Svh, SvhRowType> {

    @Override
    public SvhRowType convert(Svh entity) {
        if (entity == null) {
            return null;
        }

        SvhRowType dto = new SvhRowType();

        dto.setADRS(entity.getAdrs());
        dto.setADROWN(entity.getAdrown());
        dto.setIAKCIZ(entity.getIakciz());
        dto.setAREAS(Double.toString(entity.getAreas()));
        dto.setASOVAM(entity.getAsovam());

        if (entity.getDateMod() != null) {
            dto.setDATEMOD(new LocalDate(entity.getDateMod().getTime()));
        }

        if (entity.getDBegin() != null) {
            dto.setDBEGIN(new LocalDate(entity.getDBegin().getTime()));
        }

        if (entity.getDEnd() != null) {
            dto.setDEND(new LocalDate(entity.getDEnd().getTime()));
        }

        if (entity.getDIsl() != null) {
            dto.setDISL(new LocalDate(entity.getDIsl().getTime()));
        }

        dto.setEMAIL(entity.getEmail());
        dto.setINN(entity.getInn());
        dto.setITAM(entity.getItam());
        dto.setATA(entity.getAta());
        dto.setKODT(entity.getKodt());
        dto.setKPP(entity.getKpp());
        dto.setNLIC(entity.getNlic());
        dto.setNAMT(entity.getNamt());

        dto.setOGRN(entity.getOgrn());
        dto.setOWNER(entity.getOwner());
        dto.setTELEFON(entity.getTelefon());
        dto.setTELEFS(entity.getTelefs());
        dto.setPR441PRIL3(entity.getPr441pril3());
        dto.setPRPER(entity.getPrPer());
        dto.setST(entity.getSt());
        dto.setIDEL(entity.getIdel());
        dto.setTYPEDOC(entity.getTypDoc());
        dto.setTYPES(entity.getTypes());
        dto.setVIDTRANS(entity.getVidtrans());
        dto.setVOLUME(Double.toString(entity.getVolume()));
        dto.setWEBSITE(entity.getWebsite());

        return dto;
    }
}
