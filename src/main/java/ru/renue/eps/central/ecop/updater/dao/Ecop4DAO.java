package ru.renue.eps.central.ecop.updater.dao;

import org.springframework.stereotype.Repository;
import ru.renue.eps.central.ecop.updater.entities.Ecop4;

/**
 * Created by kgn on 05.03.2015.
 */
@Repository
public class Ecop4DAO extends AbstractEcopDAO<Ecop4> {

    public Ecop4DAO() {
        super(Ecop4.class);
    }
}
