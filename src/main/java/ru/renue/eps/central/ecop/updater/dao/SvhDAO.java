package ru.renue.eps.central.ecop.updater.dao;

import org.springframework.stereotype.Repository;
import ru.renue.eps.central.ecop.updater.entities.Svh;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class SvhDAO {

    @PersistenceContext
    private EntityManager em;

    public List<Svh> findAll() {
        return em.createQuery("select e from Svh e", Svh.class).getResultList();
    }
}
