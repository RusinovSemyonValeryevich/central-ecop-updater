package ru.renue.eps.central.ecop.updater.dao;

import org.springframework.stereotype.Repository;
import ru.renue.eps.central.ecop.updater.entities.D44sfoiv;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by kgn on 23.10.2015.
 */
@Repository
public class D44sfoivDAO {

    @PersistenceContext
    private EntityManager em;

    public List<D44sfoiv> findAll() {
        TypedQuery<D44sfoiv> query = em.createQuery("select e from " + D44sfoiv.class.getSimpleName() + " e", D44sfoiv.class);
        return query.getResultList();
    }
}
