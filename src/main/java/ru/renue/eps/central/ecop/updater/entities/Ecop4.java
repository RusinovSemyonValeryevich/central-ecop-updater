package ru.renue.eps.central.ecop.updater.entities;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "ECOP4")
@Data
public class Ecop4 extends AbstractEcop {
    @Column(name = "GUARKIND", length = 2)
    private String guarkind;

    @Column(name = "NUMBDOCTP", length = 25)
    private String numbdoctp;

    @Column(name = "DATBDOCTP")
    @Temporal(TemporalType.DATE)
    private Date datbdoctp;

    @Column(name = "BEGDOCTP")
    @Temporal(TemporalType.DATE)
    private Date begdoctp;

    @Column(name = "ENDDOCTP")
    @Temporal(TemporalType.DATE)
    private Date enddoctp;

    @Column(name = "AMOUNT", precision = 20, scale = 2)
    private BigDecimal amount;
}
