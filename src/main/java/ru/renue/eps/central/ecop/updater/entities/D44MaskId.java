package ru.renue.eps.central.ecop.updater.entities;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 31.01.2020
 * Time: 11:12
 */
@Data
public class D44MaskId implements Serializable {

    private String kod;

    private Integer nMsk;
}
