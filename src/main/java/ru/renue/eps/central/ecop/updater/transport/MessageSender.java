package ru.renue.eps.central.ecop.updater.transport;

import com.ibm.mq.jms.JMSC;
import com.ibm.mq.jms.MQQueue;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.BytesMessage;
import javax.jms.JMSException;

/**
 * Created by kgn on 04.03.2015.
 */
@Service
public class MessageSender {

    private JmsTemplate jmsTemplate;

    public MessageSender(final JmsTemplate jmsTemplate) {this.jmsTemplate = jmsTemplate;}

    public void send(final byte[] message, TransportInfo transportInfo) throws TransportException {
        try {
            MQQueue mqQueue = getDestination(transportInfo);
            jmsTemplate.send(mqQueue, session -> {
                final BytesMessage outputMessage = session.createBytesMessage();
                outputMessage.writeBytes(message);

                fillJMSHeaders(outputMessage);

                return outputMessage;
            });
        }
        catch (Exception ex) {
            throw new TransportException("Не удалось отправить сообщение", ex);
        }
    }

    private static MQQueue getDestination(TransportInfo transportInfo) throws JMSException {
        MQQueue mqQueue = new MQQueue();

        mqQueue.setBaseQueueName(transportInfo.getQueue());
        mqQueue.setPersistence(JMSC.MQJMS_PER_PER); // 1
        mqQueue.setBaseQueueManagerName(transportInfo.getQueueManager());
        mqQueue.setTargetClient(JMSC.MQJMS_CLIENT_NONJMS_MQ);

        return mqQueue;
    }

    private static void fillJMSHeaders(BytesMessage outputMessage) throws JMSException {
        outputMessage.setIntProperty("JMS_IBM_Encoding", 546);
        outputMessage.setIntProperty("JMS_IBM_Character_Set", 1208);
        outputMessage.setJMSDeliveryMode(1);
    }
}
