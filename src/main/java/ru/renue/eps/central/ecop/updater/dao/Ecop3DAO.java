package ru.renue.eps.central.ecop.updater.dao;


import org.springframework.stereotype.Repository;
import ru.renue.eps.central.ecop.updater.entities.Ecop3;

/**
 * Created by kgn on 05.03.2015.
 */
@Repository
public class Ecop3DAO extends AbstractEcopDAO<Ecop3> {

    public Ecop3DAO() {
        super(Ecop3.class);
    }
}
