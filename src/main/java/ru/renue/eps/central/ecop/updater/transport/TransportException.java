package ru.renue.eps.central.ecop.updater.transport;

/**
 * Created by kgn on 04.03.2015.
 */
public class TransportException extends Exception {

    TransportException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
