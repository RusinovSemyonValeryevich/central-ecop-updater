package ru.renue.eps.central.ecop.updater;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableScheduling
@EnableJms
public class CentralEcopUpdaterApplication {

    public static void main(String[] args) {
        SpringApplication.run(CentralEcopUpdaterApplication.class, args);
    }

}
