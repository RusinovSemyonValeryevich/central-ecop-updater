package ru.renue.eps.central.ecop.updater.dao;

import ru.renue.eps.central.ecop.updater.entities.AbstractEcop;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by kgn on 05.03.2015.
 */
public abstract class AbstractEcopDAO<T extends AbstractEcop> {
    @PersistenceContext
    private EntityManager em;

    protected Class<T> entityClass;

    AbstractEcopDAO(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public List<T> findAll() {
        TypedQuery<T> query = em.createQuery("select e from " + entityClass.getSimpleName() + " e", entityClass);
        return query.getResultList();
    }
}
