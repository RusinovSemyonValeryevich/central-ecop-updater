package ru.renue.eps.central.ecop.updater.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kgn on 05.03.2015.
 */

@MappedSuperclass
@Data
public abstract class AbstractEcop {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", allocationSize = 1, sequenceName = "ecop_sequence")
    @Column(name = "ID")
    private Long id;

    @Column(name = "ST", length = 2)
    private String st;

    @Column(name = "NLIC", length = 25)
    private String nlic;

    @Column(name = "PR_PER", length = 1)
    private String prPer;

    @Column(name = "DBEGIN")
    @Temporal(TemporalType.DATE)
    private Date dBegin;
}
