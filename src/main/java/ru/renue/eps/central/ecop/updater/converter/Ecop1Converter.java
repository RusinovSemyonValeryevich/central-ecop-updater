package ru.renue.eps.central.ecop.updater.converter;

import org.joda.time.LocalDate;
import ru.kontur.fts.eps.schemas.nci.ecopcatalog.ECOP1RowType;
import ru.renue.eps.central.ecop.updater.entities.Ecop1;

/**
 * Created by kgn on 05.03.2015.
 */
public class Ecop1Converter extends Entity2DtoConverter<Ecop1, ECOP1RowType> {

    @Override
    public ECOP1RowType convert(Ecop1 entity) {
        if (entity == null) {
            return null;
        }
        ECOP1RowType dto = new ECOP1RowType();
        dto.setCITY(entity.getCity());
        if (entity.getDBegin() != null) {
            dto.setDBEGIN(new LocalDate(entity.getDBegin().getTime()));
        }
        dto.setINN(entity.getInn());
        dto.setKPP(entity.getKpp());
        dto.setNLIC(entity.getNlic());
        dto.setOWNER(entity.getOwner());
        dto.setPOST(entity.getPost());
        dto.setPRPER(entity.getPrPer());
        dto.setST(entity.getSt());
        dto.setSTREET(entity.getStreet());
        dto.setSUBD(entity.getSubD());

        return dto;
    }
}
