package ru.renue.eps.central.ecop.updater.processing;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.kontur.fts.eps.schemas.nci.d44mask.D44MaskCatalogType;
import ru.renue.eps.central.ecop.updater.converter.D44MaskConverter;
import ru.renue.eps.central.ecop.updater.converter.D44mtnConverter;
import ru.renue.eps.central.ecop.updater.dao.D44MaskDAO;
import ru.renue.eps.central.ecop.updater.dao.D44mtnDAO;
import ru.renue.eps.central.ecop.updater.transport.MessageSender;

import java.util.List;
import java.util.UUID;

/**
 * Created by azhebko on 29.01.2016.
 */
@Service
public class D44MaskUpdater extends Updater {

    private final D44MaskDAO d44MaskDAO;

    private final D44mtnDAO d44mtnDAO;

    public D44MaskUpdater(final D44MaskDAO d44MaskDAO, final D44mtnDAO d44mtnDAO,
                          final @Qualifier("d44MaskRoutingInfos") List<RoutingInfo> routingInfos,
                          final MessageConstructor messageConstructor, final MessageSender messageSender) {
        super(routingInfos, messageConstructor, messageSender, "ADM.00102");
        this.d44MaskDAO = d44MaskDAO;
        this.d44mtnDAO = d44mtnDAO;
        logger = LoggerFactory.getLogger(D44MaskUpdater.class);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Scheduled(cron = "0 5 0 * * *")
    public void process() {
        D44MaskCatalogType catalog = new D44MaskCatalogType();
        catalog.setDocumentID(UUID.randomUUID().toString());
        catalog.setD44MaskList(new D44MaskConverter().convert(d44MaskDAO.findAll()));
        catalog.setD44mtnList(new D44mtnConverter().convert(d44mtnDAO.findAll()));
        update(catalog);
    }
}
