package ru.renue.eps.central.ecop.updater.transport;

import lombok.Data;

/**
 * Created by kgn on 04.03.2015.
 */
@Data
public class TransportInfo {

    private String queue;

    private String queueManager;

    @Override
    public String toString() {
        return String.format("WMQ://%s/%s", queueManager, queue);
    }
}
