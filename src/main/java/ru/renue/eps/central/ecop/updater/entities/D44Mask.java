package ru.renue.eps.central.ecop.updater.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by azhebko on 29.01.2016.
 */
@Entity
@Table(name = "D44_MASK")
@IdClass(D44MaskId.class)
@Data
public class D44Mask {

    @Id
    @Column(name = "KOD")
    private String kod;

    @Column(name = "K_MASKA")
    private String kMaska;

    @Id
    @Column(name = "N_MSK")
    private Integer nMsk;

    @Column(name = "SID_SMEV")
    private String sidSmev;

    @Column(name = "NAME_MSK")
    private String nameMsk;

    @Column(name = "DSCR_MSK")
    private String dscrMsk;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATBEG")
    private Date datbeg;

    @Column(name = "NUMBEGDOC")
    private String numbegdoc;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATBEGDOC")
    private Date datbegdoc;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATEND")
    private Date datend;

    @Column(name = "NUMENDDOC")
    private String numenddoc;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATENDDOC")
    private Date datenddoc;

    @Column(name = "PRIZNAK")
    private String priznak;

    @Column(name = "P_DOP")
    private String pDop;
}
