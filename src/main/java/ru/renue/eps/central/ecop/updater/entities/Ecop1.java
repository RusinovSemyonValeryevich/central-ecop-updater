package ru.renue.eps.central.ecop.updater.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ECOP1")
@Data
public class Ecop1 extends AbstractEcop {

    @Column(name = "OWNER", length = 255)
    private String owner;

    @Column(name = "INN", length = 20)
    private String inn;

    @Column(name = "KPP", length = 9)
    private String kpp;

    @Column(name = "POST", length = 9)
    private String post;

    @Column(name = "SUBD", length = 50)
    private String subD;

    @Column(name = "CITY", length = 35)
    private String city;

    @Column(name = "STREET", length = 50)
    private String street;
}
