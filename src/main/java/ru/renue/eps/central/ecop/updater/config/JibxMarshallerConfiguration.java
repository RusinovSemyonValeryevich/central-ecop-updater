package ru.renue.eps.central.ecop.updater.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.jibx.JibxMarshaller;
import ru.kontur.fts.eps.schemas.common.EnvelopeType;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 31.01.2020
 * Time: 9:28
 */
@Configuration
public class JibxMarshallerConfiguration {

    @Bean
    public Marshaller marshaller() {
        JibxMarshaller marshaller = new JibxMarshaller();
        marshaller.setTargetClass(EnvelopeType.class);
        return marshaller;
    }
}
