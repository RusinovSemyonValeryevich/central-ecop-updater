package ru.renue.eps.central.ecop.updater.processing;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.kontur.fts.eps.schemas.nci.d44sfoiv.D44SFOIVCatalogType;
import ru.renue.eps.central.ecop.updater.converter.D44sfoivConverter;
import ru.renue.eps.central.ecop.updater.dao.D44sfoivDAO;
import ru.renue.eps.central.ecop.updater.entities.D44sfoiv;
import ru.renue.eps.central.ecop.updater.transport.MessageSender;

import java.util.List;
import java.util.UUID;

/**
 * Created by kgn on 23.10.2015.
 */
@Service
public class D44foivUpdater extends Updater {

    private final D44sfoivDAO dao;

    public D44foivUpdater(final D44sfoivDAO dao, final @Qualifier("d44foivRoutingInfos") List<RoutingInfo> routingInfos,
                          final MessageConstructor messageConstructor, final MessageSender messageSender) {
        super(routingInfos, messageConstructor, messageSender, "ADM.00101");
        this.dao = dao;
        logger = LoggerFactory.getLogger(D44foivUpdater.class);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Scheduled(cron = "0 5 0 * * *")
    public void process() {
        List<D44sfoiv> entities = dao.findAll();
        D44sfoivConverter converter = new D44sfoivConverter();
        D44SFOIVCatalogType catalog = new D44SFOIVCatalogType();
        catalog.setDocumentID(UUID.randomUUID().toString());
        catalog.setD44SFOIVList(converter.convert(entities));
        update(catalog);
    }
}
